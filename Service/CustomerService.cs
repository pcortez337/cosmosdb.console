﻿using CosmosDB.Console.Model;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CosmosDB.Console.Service
{
    public class CustomerService
    {

        public CustomerService()
        {

        }

        public async Task<Boolean> CreateCustomerAsync(DocumentClient client, Customer newCustomer)
        {
            try
            {
                await client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri("ChampionProductsCosmosDB", "Customer2"), newCustomer);
            }
            catch (Exception ex)
            {

                return false;
            }

            return true;
        }

        public async Task<Boolean> UpdateCustomerAsync(DocumentClient client, Customer customerToUpdate)
        {
            try
            {
                ResourceResponse<Document> response = await client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri("ChampionProductsCosmosDB", "Customer2", customerToUpdate.id), customerToUpdate);
            }
            catch (Exception ex)
            {

                return false;
            }

            return true;
        }

        public async Task<Boolean> DeleteCustomerAsync(DocumentClient client, Customer customerToDelete)
        {
            try
            {
                ResourceResponse<Document> response = await client.DeleteDocumentAsync(UriFactory.CreateDocumentUri("ChampionProductsCosmosDB", "Customer2", customerToDelete.id), new RequestOptions { PartitionKey = new PartitionKey(customerToDelete.id) });
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

    }
}
