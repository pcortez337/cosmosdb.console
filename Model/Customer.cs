﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CosmosDB.Console.Model
{
    public class Customer
    {
        public Customer()
        {
            addresses = new List<Address>();
            invoices = new List<Invoice>();
        }

        public string id { get; set; }
        public int customerId { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public List<Address> addresses { get; set; }
        public List<Invoice> invoices { get; set; }

    }
}
