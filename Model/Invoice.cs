﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CosmosDB.Console.Model
{
    public class Invoice
    {
        public int invoiceId { get; set; }
        public DateTime date { get; set; }
        public decimal amount { get; set; }
        public DateTime? paidDate { get; set; }
        public decimal paidAmount { get; set; }
    }

}
