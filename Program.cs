﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

using CosmosDB.Console.Model;
using System.Collections.Generic;
using System.Linq;

namespace CosmosDB.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            var connectionString = "AccountEndpoint=https://accnt-01.documents.azure.com:443/;AccountKey=mpqFKrQqu7xasseeLh3lkCJ8wnf10Xfqsh7bxRnoIszx6nawZnL2PqrSVjJz6dqSueJZa2BmhukoZpNX9sFbKA==";

            using (var client = new CosmosClient(connectionString))
            {

                var DB = client.GetDatabase("ChampionProductsCosmosDB");
                var table = client.GetContainer(DB.Id, "Customer");

                //loop through all records
                var sqlString = "Select * from c";
                var query = new QueryDefinition(sqlString);
                FeedIterator<Customer> iterator = table.GetItemQueryIterator<Customer>(sqlString);

                FeedResponse<Customer> queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();

                foreach (var item in queryResult.Resource)
                {
                    Debug.WriteLine("Name: " + item.lastName + ", " + item.firstName);
                }

                //Get records with 'Smith' as lastname
                sqlString = "SELECT * FROM c where c.lastName = 'Smith'";
                query = new QueryDefinition(sqlString);
                iterator = table.GetItemQueryIterator<Customer>(sqlString);

                queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();

                foreach (var item in queryResult.Resource)
                {
                    Debug.WriteLine("Name: " + item.lastName + ", " + item.firstName);
                }
            }
            */

            PerformCrudOps();

        }

        private static void PerformCrudOps()
        {
            ConnectionPolicy connectionPolicy = new ConnectionPolicy();
            connectionPolicy.ConnectionMode = Microsoft.Azure.Documents.Client.ConnectionMode.Direct;
            connectionPolicy.ConnectionProtocol = Protocol.Tcp;

            // Set the read region selection preference order
            connectionPolicy.PreferredLocations.Add(LocationNames.EastUS); // first preference
            connectionPolicy.PreferredLocations.Add(LocationNames.NorthEurope); // second preference
            connectionPolicy.PreferredLocations.Add(LocationNames.SoutheastAsia); // third preference

            string uri = "https://accnt-01.documents.azure.com:443/";

            var documentClient = new DocumentClient(new Uri(uri), "mpqFKrQqu7xasseeLh3lkCJ8wnf10Xfqsh7bxRnoIszx6nawZnL2PqrSVjJz6dqSueJZa2BmhukoZpNX9sFbKA==", connectionPolicy);
            var Service = new Service.CustomerService();
            /*
            //Add a new customer
            var newCustomer = new Customer()
            {
                customerId = 1,
                lastName = "Smith",
                firstName = "Tabb",
                addresses = new List<Address>() {new Address()
                                                 {
                                                    addressId=1,
                                                    addressTypeId = 1,
                                                    street1 = "100 Main Street",
                                                    street2 = "Suite 2",
                                                    city = "Dallas",
                                                    state = "TX",
                                                    zip = "75201"
                                                    },
                                                new Address()
                                                {
                                                    addressId=2,
                                                    addressTypeId = 2,
                                                    street1 = "P.O. Box 123",
                                                    street2 = null,
                                                    city = "Dallas",
                                                    state = "TX",
                                                    zip = "75202"
                                                }
                },
                invoices = new List<Invoice>() {new Invoice()
                                                {
                                                    invoiceId = 1,
                                                    date = DateTime.Parse("2019-04-28"),
                                                    amount = decimal.Parse("101.25"),
                                                    paidDate = null,
                                                    paidAmount = 0,
                                                },
                                                new Invoice()
                                                {
                                                    invoiceId = 2,
                                                    date = DateTime.Parse("2019-05-01"),
                                                    amount = decimal.Parse("23.54"),
                                                    paidDate = DateTime.Parse("2019-05-02"),
                                                    paidAmount = decimal.Parse("23.54")
                                                }

                }
            };

            var wasSuccessfullyAdded = Task.Run(() => Service.CreateCustomerAsync(documentClient, newCustomer)).GetAwaiter().GetResult();

            //Add another customer
            var newCustomer2 = new Customer()
            {
                customerId = 2,
                lastName = "Jones",
                firstName = "Tammy",
                addresses = new List<Address>() {new Address()
                                                 {
                                                    addressId=3,
                                                    addressTypeId = 1,
                                                    street1 = "111 Main Street",
                                                    street2 = "Suite 25",
                                                    city = "Dallas",
                                                    state = "TX",
                                                    zip = "75201"
                                                    },
                                                new Address()
                                                {
                                                    addressId=4,
                                                    addressTypeId = 2,
                                                    street1 = "P.O. Box 123445",
                                                    street2 = null,
                                                    city = "Dallas",
                                                    state = "TX",
                                                    zip = "75202"
                                                }
                },
                invoices = new List<Invoice>() {new Invoice()
                                                {
                                                    invoiceId = 3,
                                                    date = DateTime.Parse("2019-02-28"),
                                                    amount = decimal.Parse("121.25"),
                                                    paidDate = null,
                                                    paidAmount = 10,
                                                },
                                                new Invoice()
                                                {
                                                    invoiceId = 4,
                                                    date = DateTime.Parse("2019-04-01"),
                                                    amount = decimal.Parse("223.54"),
                                                    paidDate = DateTime.Parse("2019-05-02"),
                                                    paidAmount = decimal.Parse("223.54")
                                                }

                }
            };

            wasSuccessfullyAdded = Task.Run(() => Service.CreateCustomerAsync(documentClient, newCustomer2)).GetAwaiter().GetResult();

            //Add another customer
            var newCustomer3 = new Customer()
            {
                customerId = 3,
                lastName = "Struck",
                firstName = "Charles",
                addresses = new List<Address>() {new Address()
                                                 {
                                                    addressId=5,
                                                    addressTypeId = 1,
                                                    street1 = "3000 Lewis Drive",
                                                    street2 = "Suite 200",
                                                    city = "Dallas",
                                                    state = "TX",
                                                    zip = "75201"
                                                    }

                },
                invoices = new List<Invoice>() {new Invoice()
                                                {
                                                    invoiceId = 5,
                                                    date = DateTime.Parse("2019-02-20"),
                                                    amount = decimal.Parse("1121.25"),
                                                    paidDate = null,
                                                    paidAmount = decimal.Parse("100.22"),
                                                }

                }
            };

            wasSuccessfullyAdded = Task.Run(() => Service.CreateCustomerAsync(documentClient, newCustomer3)).GetAwaiter().GetResult();
            */

            //Update a customer
            //getting the customer I want to update.
            var option = new FeedOptions { EnableCrossPartitionQuery= true };

        Customer customerToUpdate = documentClient.CreateDocumentQuery<Customer>(UriFactory.CreateDocumentCollectionUri("ChampionProductsCosmosDB", "Customer2"), option)
            .Where(c => c.id == "452f4e03-9609-401c-8f74-8070f72fa6e0")
            .AsEnumerable()
            .FirstOrDefault();

            if (customerToUpdate != null)
            {
                customerToUpdate.firstName = "James"; //make a change to any property in the customer object
                customerToUpdate.addresses.FirstOrDefault().state = "MN"; //make a change to any property in the customer object

                var wasSuccessfullyUpdated = Task.Run(() => Service.UpdateCustomerAsync(documentClient, customerToUpdate)).GetAwaiter().GetResult();
            }


            //delete a customer
            //getting the customer I want to delete.
            Customer customerToDelete = documentClient.CreateDocumentQuery<Customer>(UriFactory.CreateDocumentCollectionUri("ChampionProductsCosmosDB", "Customer2"), option)
            .Where(c => c.id == "7002fe1a-1357-486d-be29-62dd1d601586")
            .AsEnumerable()
            .LastOrDefault();

            if (customerToDelete != null)
            {
                var wasSuccessfullyDeleted = Task.Run(() => Service.DeleteCustomerAsync(documentClient, customerToDelete)).GetAwaiter().GetResult();
            }

        }

    }
}
